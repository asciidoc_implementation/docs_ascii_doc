.Аварийная индикация
* xref:alarm/alerts.adoc[Аварии]
* xref:alarm/troubleshooting.adoc[Диагностика неисправностей]

.Базовые сведения
* xref:basics/functionalities.adoc[Функциональные возможности]
* xref:basics/general.adoc[Общие сведения]
* xref:basics/high_availability.adoc[Резервирование]
* xref:basics/internal_architecture.adoc[Внутренняя архитектура]
* xref:basics/lifecycle.adoc[Жизненный цикл]
* xref:basics/network_architecture.adoc[Сетевая архитектура]
* xref:basics/requirements_hw_sw.adoc[Системные требования]
* xref:basics/roadmap.adoc[Оперативные планы]
* xref:basics/scaling.adoc[Масштабирование]
* xref:basics/specifications.adoc[Спецификации и рекомендации]

.Эксплуатация, администрирование и техническое обслуживание
* xref:oam/api.adoc[HTTP API]
* xref:oam/backup_recovery.adoc[Резервное копирование]
* xref:oam/installation.adoc[Установка]
* xref:oam/performance_kpi.adoc[KPI]
* xref:oam/system_management.adoc[Управление]
* xref:oam/update.adoc[Обновление]